import java.util.ArrayList;

/**
 * Created by dogor on 02.04.2017.
 */
public class Address {

    private String Country;
    private String Region;
    private String City;
    private String Street;
    private String HouseId;
    private String HouseSubId;
    private String DoorId;
    private String[] tmparr;


    public Address(String string){

        ParserQ parser = new ParserQ();

        ArrayList<String> tmp = new ArrayList<String>();
        tmp = parser.parse(string);

        tmparr = tmp.toArray(new String[tmp.size()]);

        this.Country = tmparr[0];
        this.Region = tmparr[1];
        this.City = tmparr[2];
        this.Street = tmparr[3];
        this.HouseId = tmparr[4];
        this.HouseSubId = tmparr[5];
        this.DoorId = tmparr[6];


    }

    public Address(){ }


    public String getCountry(){

        return this.Country;

    }

    public String getRegion(){

        return this.Region;

    }

    public String getCity(){

        return this.City;

    }

    public String getHouseId(){

        return this.HouseId;

    }

    public String getHouseSubId(){

        return this.HouseSubId;

    }

    public String getDoorId(){

        return this.DoorId;

    }

    public String getStreet(){

        return this.Street;

    }

    @Override

    public String toString(){
        return  "Country: "+ getCountry() + "  Region: " + getRegion()
                + "  City: " + getCity() + "  HouseId: " + getHouseId()
                + "  HouseSubId: " + getHouseSubId() + "  DoorId: " +  getDoorId() + "\n";
    }

}

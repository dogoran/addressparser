import java.util.ArrayList;

/**
 * Created by dogor on 02.04.2017.
 */
public class Main {
    public static void main(String[] args) {

        ArrayList<Address> AddressList = new ArrayList();
        AddressList.add(new Address("НеСтрана,Чубака,Пушкина,14,56,123,312"));
        AddressList.add(new Address("USA,NY,WS,2,16,12,2"));
        AddressList.add(new Address("Africa,Tanzania,KS,15,4,122,12"));
        AddressList.add(new Address("UA,DONETSK,DONETSK,44,2,14,2"));

        showList(AddressList);

    }

    public static void showList(ArrayList<Address> list){

        for (Address a : list){
            System.out.println(a.toString());
        }

    }
}
